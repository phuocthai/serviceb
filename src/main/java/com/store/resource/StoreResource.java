package com.store.resource;

import java.net.URI;
import java.net.URISyntaxException;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.store.service.StoreService;

@Path("stores")
public class StoreResource {

	@Inject
	@RestClient
	private StoreService storeService;

	@GET
	@Path("/books")
	public Response getAllBooks() {
		return storeService.getAll();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllBook() throws URISyntaxException {
		URI apiURI = new URI("http://localhost:8080/servicea/api/books");
		StoreService client = RestClientBuilder.newBuilder().baseUri(apiURI).build(StoreService.class);
		return client.getAll();
	}

}
