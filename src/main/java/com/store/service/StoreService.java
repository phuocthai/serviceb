package com.store.service;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient
public interface StoreService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll();
	
}
