# Build
mvn clean package && docker build -t com.microprofile/serviceb .

# RUN

docker rm -f serviceb || true && docker run -d -p 8080:8080 -p 4848:4848 --name serviceb com.microprofile/serviceb 